# to be run from the project root

# install ansible
sudo yum install epel-release
sudo yum install ansible

# configure machine
cd env/Ansible/provision
ansible-playbook -K playbook_centos.yml # -K = allow to act as root if needed

# install dependencies
cd -
yarn install
sh scripts/utils/link_custom_lib.sh

# init db
source config/development.env && yarn resetdb
