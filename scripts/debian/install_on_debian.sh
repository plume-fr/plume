# to be run from the project root

# install ansible
sudo apt-get install ansible

# configure machine
cd env/Ansible/provision
ansible-playbook -K playbook_debian.yml # -K = allow to act as root if needed

# install dependencies
cd -
yarn install

# init db
source config/development.env && yarn resetdb
