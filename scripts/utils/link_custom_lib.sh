#!/usr/bin/env bash

cd ../custom_package/editoria/packages/wax
yarn link
cd -
yarn link pubsweet-component-wax

cd ../custom_package/editoria/packages/dashboard
yarn link
cd -
yarn link pubsweet-component-editoria-dashboard

cd ../custom_package/editoria/packages/navigation
yarn link
cd -
yarn link pubsweet-component-editoria-navigation

cd ../custom_package/editoria/packages/bookbuilder
yarn link
cd -
yarn link pubsweet-component-bookbuilder

cd ../custom_package/editoria/packages/parser
yarn link
cd -
cd ../custom_package/editoria/packages/api
yarn link plume-book-parser

cd ../custom_package/editoria/packages/api
yarn link
cd -
yarn link editoria-api

cd ../custom_package/editoria/packages/paged-viewer
yarn link
cd -
yarn link pubsweet-component-pagedjs-viewer

cd ../custom_package/editoria/packages/Login
yarn link
cd -
yarn link editoria-component-login

cd ../custom_package/editoria/packages/Signup
yarn link
cd -
yarn link editoria-component-signup

cd ../custom_package/editoria/packages/i18n/
yarn link
cd -
yarn link editoria-i18n