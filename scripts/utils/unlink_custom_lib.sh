#!/usr/bin/env bash

yarn unlink wax-editor-core

yarn unlink wax-editor-react

yarn unlink pubsweet-component-editoria-dashboard

yarn unlink pubsweet-component-editoria-navigation

yarn unlink pubsweet-component-bookbuilder

yarn unlink plume-book-parser

yarn unlink editoria-api

yarn unlink pubsweet-component-pagedjs-viewer

yarn unlink editoria-component-login

yarn unlink editoria-component-signup

yarn unlink editoria-i18n

yarn install --force