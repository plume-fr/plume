import 'regenerator-runtime/runtime'

import React from 'react'
import ReactDOM from 'react-dom'
import {IntlProvider, FormattedMessage} from 'react-intl';
import {frFR, enUS} from 'editoria-i18n'
import { hot } from 'react-hot-loader'

import createHistory from 'history/createBrowserHistory'

import { Root } from 'pubsweet-client'

// Modals
import ModalProvider from 'editoria-common/src/ModalProvider'
import AddBookModal from 'pubsweet-component-editoria-dashboard/src/modals/AddBookModal'
import DeleteBookModal from 'pubsweet-component-editoria-dashboard/src/modals/DeleteBookModal'
import ArchiveBookModal from 'pubsweet-component-editoria-dashboard/src/modals/ArchiveBookModal'
import DeleteBookComponentModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/DeleteBookComponentModal'
import BookTeamManagerModal from 'pubsweet-component-bookbuilder/src/TeamManager/ConnectedTeamManager'
import WarningModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/WarningModal'
import ErrorModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/ErrorModal'
import UnlockModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/UnlockModal'
import MetadataModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/MetadataModal'
import WorkflowModal from 'pubsweet-component-bookbuilder/src/ui/src/modals/WorkflowModal'
import UnlockedModal from 'pubsweet-component-wax/src/modals/UnlockedModal'

import theme from './theme'

import routes from './routes'

const history = createHistory()
const modals = {
  addBook: AddBookModal,
  deleteBook: DeleteBookModal,
  archiveBook: ArchiveBookModal,
  deleteBookComponent: DeleteBookComponentModal,
  bookTeamManager: BookTeamManagerModal,
  warningModal: WarningModal,
  unlockModal: UnlockModal,
  metadataModal: MetadataModal,
  workflowModal: WorkflowModal,
  errorModal: ErrorModal,
  unlockedModal: UnlockedModal,
}
const rootEl = document.getElementById('root')

const MESSAGES = {
  'fr-FR': frFR,
  'en-US': enUS,
}

const LOCALE = 'fr-FR'

ReactDOM.render(
    <IntlProvider
        locale= {LOCALE}
        messages={MESSAGES[LOCALE]}
    >
      <ModalProvider modals={modals}>
        <Root history={history} routes={routes} theme={theme} />
      </ModalProvider>,
    </IntlProvider>,
    rootEl,
)

export default hot(module)(Root)
