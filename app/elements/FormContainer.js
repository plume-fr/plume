import { css } from 'styled-components'
import backgroundImg from '../../static/background.jpg'

export default css `
    background-image: url(${backgroundImg});
    & > .form {
     width: 28%;
     margin-left: 36%;
     padding: 40px;
     margin-bottom: 20px;
     margin-top: 20px;
     background-color: rgba(255, 255, 255, 0.8);
    }
    & > .link-to-blog {
     margin-top: 30px;
     font-weight: bold;
    }
`