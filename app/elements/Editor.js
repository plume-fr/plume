import React,{Component} from 'react';
import Editor from '@plume-fr/plume-editor';
import ConnectedEditor from 'pubsweet-component-wax/src/ConnectedEditor'
import './Editor.css';

class EditorPlume extends Component {
    
    render() {
        const WrappedEditor = ConnectedEditor(Editor, {... this.props})
        return <WrappedEditor />
    }
}
export default EditorPlume;