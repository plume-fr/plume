# What did I do then ? 


-----------------
### "explicit title describing the error"

##### why does it happens ? 

##### what to do ?

##### Recurrent bug ?

-----------------
### "explicit title describing the error"
error: permission denied to create extension "pgcrypto"
#####why does it happens ? 
The database user doesn't have the clearance dans to create the extension
#####what to do ?
~~~~ 
sudo su postgres
psql
ALTER USER vagrant WITH SUPERUSER 
~~~~

Becarefull, you're giving a great power to your database user :D

#####occurence ?
Too many time

### error: could not open extension control file "/usr/pgsql-10/share/extension/pgcrypto.control": No such file or directory

#####why does it happens ? 
postgresql10-contrib is not installed
#####what to do ?
yum install postgresql10-contrib
#####occurence ?
Too many time

### App doesn't work on Vagrant with a sync folder shared with windows 10

#####why does it happens ? 
You have the following error when you try to launch the application,
 'Error [ERR_STREAM_DESTROYED]: Cannot call write after a stream was destroyed'

#####what to do ?
change the folder to write logs in in the configuration of winston. (in config/development.js)

#####occurence ?
once

### app would not start after linking a new custom library (OR if a used-to-be-working linked library seems to fail)

##### why does it happens ? 
the folder in node_modules and the one linked are identical except for their own node_modules folder, the official one had its own folder containing dependencies

##### what to do ?
run yarn install inside the linked folder to install the required dependencies that aren't available in the main project

##### Recurrent bug ?
editoria/packages/api lib 3 times (so much lost time !!!)

-----------------

### when using yarn install in the main project an error occured while building/installing the bcrypt library 

##### why does it happens ? 
as of june 2019, the bcrypt library need a specific node version to work properly

##### what to do ?
use n or another package manager to change nodejs to 11.15.0 (not 11.13 nor 12)

##### Recurrent bug ?
happened twice

-----------------

### CENTOS : fail to clone pubsweet/html-epub during yarn install

##### why does it happens ? 
you need to update git to >= 2.x

##### what to do ?
uninstall the current git package and use : https://stackoverflow.com/questions/21820715/how-to-install-latest-version-of-git-on-centos-7-x-6-x
*note: this is handled by the ansible script*

##### Recurrent bug ?
happened twice

-----------------

### CENTOS : g++ command not found

##### why does it happens ? 
a library is missing to build a dependency

##### what to do ?
yum install gcc-c++  ()
*note: this is handled by the ansible script*

##### Recurrent bug ?
happened twice (but should not come back with ansible)

-----------------

### Yarn Server : app restart again and again

##### why does it happens ? 
if you imported some new code, the code may not be compatible with editoria node version 

##### what to do ?
run the code with the node command in a prompt. 
example : replace all import by require

##### Recurrent bug ?
no

-----------------

### Yarn Server : error : authentication failed for user

##### why does it happens ? 
invalid env variable

##### what to do ?
run source config/development.env

##### Recurrent bug ?
yes when ran manually

-----------------

### TypeError: User is not a constructor

##### why does it happens ? 
no idea but you can assume something went wrong during the yarn install

##### what to do ?
fix the yarn install

##### Recurrent bug ?
3 times

-----------------

### fail to build bcrypt depency

##### why does it happens ? 
missing package

##### what to do ?
debian : apt install build-essential
centos : yum install gcc-c++  ()
*note: this is handled by the ansible script* 

##### Recurrent bug ?
twice (but should not come back with ansible)

-----------------

### Counter is not reseted as specified

##### why does it happens ? 
counter reset for a given selector must be set once and can't be defined for each counter along the file

##### what to do ?
replace 
````
body {
    counter-reset: h1;
}

 [....]
 
body {
    counter-reset: recommandationCnt;
}
````
by 
````
body {
    counter-reset: recommandationSectionCnt recommandationCnt h1;
}
````


##### Recurrent bug ?
nope

-----------------