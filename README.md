# Plume

## Installation using a VM 

- Clone the repo
- Add a development.env file in the config folder and complete it following the guide bellow
- From the root of the repo, run 
```sh scripts/install.sh```

By default your instance will be accessible at "http://localhost:3004"

Next time, only run 
```sh scripts/start_on_existing_vm.sh```

## Installation on your physical machine

### Debian
- Clone the repo
- Add a development.env file in the config folder and complete it following the guide bellow
- Update the "env/Ansible/provision/roles/editoria/tasks/main.yml" file by replacing "/var/www/plume" by the path to your project
- To setup the environnement, from env/Ansible/provision/ run 
```ansible-playbook --ask-sudo-pass playbook.yml```
- To start the service the first time, from the root of the repo, run 
```sh scripts/on_going/start_server_first_time.sh```

Once the next start up, use 
```sh scripts/on_going/start_server.sh```

### Centos
Update the "env/Ansible/provision/playbook.yml" file by replacing "debian" by "centos" then follow the Debian guide.

## Configure your development.env file

```
export PUBSWEET_SECRET='' (*) (**)
export POSTGRES_USER='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export POSTGRES_PASSWORD='' (*) (***) (by from both docker-compose.yml and pubsweet server)
export POSTGRES_HOST='' (-)
export POSTGRES_DB='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export POSTGRES_PORT='' (*) (***) (used by both docker-compose.yml and pubsweet server)
export SERVER_PORT='' (**)
export INK_ENDPOINT='' (*) (**)
export INK_USERNAME='' (*) (**)
export INK_PASSWORD='' (*) (**)
export INK_EDITORIA_TYPESCRIPT='' (*) (**)
export MAILER_USER='' (*) (**)
export MAILER_PASSWORD='' (*) (**)
export MAILER_SENDER='' (*) (**)
export MAILER_HOSTNAME='' (*) (**)
export PASSWORD_RESET_URL='' (*) (**)
export PASSWORD_RESET_SENDER='' (*) (**)
export NODE_ENV='' (**)
```
(*)Required for the application to be functional

(-) Optional

(**) This key-value pairs could be either declared as env variables or either in the corresponding config file e.g. `local-development.json`, `development.json`, etc

(***) These fields should by any means exist in the env source file for the correct initialization of the docker container which holds the database of the application

You can ask for a default version of the development.env file on the mattermost of the community